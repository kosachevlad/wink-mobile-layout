const path = require('path')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const autoprefixer = require('autoprefixer')

module.exports = {
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'public')
  },
  plugins: [
    new HTMLWebpackPlugin({
      title: 'Wink',
      template: './src/index.html'
    }),
    new HTMLWebpackPlugin({
      title: 'Whats new',
      filename: 'whats-new.html',
      template: './src/whats-new.html'
    }),
    new HTMLWebpackPlugin({
      title: 'Dashboard',
      filename: 'dashboard.html',
      template: './src/dashboard.html'
    }),
    new HTMLWebpackPlugin({
      title: 'Menu open',
      filename: 'menu-open.html',
      template: './src/menu-open.html'
    }),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({ 
      patterns: [
        {
          from: path.resolve(__dirname, 'src/images/favicon.ico'),
          to:  path.resolve(__dirname, 'public', 'img')
        },
        {
          from: "src/images/svg",
          to: path.resolve(__dirname, 'public', 'img/[name].[ext]'),
          toType: "template",
        },
        {
          from: "src/images/png",
          to: path.resolve(__dirname, 'public', 'img/[name].[ext]'),
          toType: "template",
        },
      ]
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    })
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              postcssOptions: {
                plugins: [autoprefixer()],
              },
            },
          },
        ],
      },
      {
        test: /\.less$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              postcssOptions: {
                plugins: [autoprefixer()],
              },
            },
          },
          {
            loader: 'less-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2|svg)(\?.*)?$/,
        exclude: [path.resolve(__dirname, 'src/images/')],
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '/fonts/',
              publicPath: path.resolve(__dirname, 'public/fonts')
            },
          },
        ],
      },
      {
        test: /\.(jpeg|jpg|png|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '/img/',
              publicPath: path.resolve(__dirname, 'public/img')
            },
            
          }
        ]
      }
    ],
    
  },
  devtool: 'source-map',
} 